﻿namespace PBT.ComplexFigure
{
    partial class FormComplexFigure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkShowFigure = new System.Windows.Forms.CheckBox();
            this.btExit = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.btPlay = new System.Windows.Forms.Button();
            this.btClear = new System.Windows.Forms.Button();
            this.btScoring = new System.Windows.Forms.Button();
            this.inkPicture1 = new Microsoft.Ink.InkPicture();
            this.lbScore = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.inkPicture1)).BeginInit();
            this.SuspendLayout();
            // 
            // chkShowFigure
            // 
            this.chkShowFigure.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkShowFigure.Checked = true;
            this.chkShowFigure.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkShowFigure.Location = new System.Drawing.Point(4, 19);
            this.chkShowFigure.Name = "chkShowFigure";
            this.chkShowFigure.Size = new System.Drawing.Size(80, 30);
            this.chkShowFigure.TabIndex = 6;
            this.chkShowFigure.Text = "Show Figure";
            this.chkShowFigure.UseVisualStyleBackColor = true;
            this.chkShowFigure.CheckedChanged += new System.EventHandler(this.chkShowFigure_CheckedChanged);
            // 
            // btExit
            // 
            this.btExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btExit.Location = new System.Drawing.Point(997, 19);
            this.btExit.Name = "btExit";
            this.btExit.Size = new System.Drawing.Size(80, 30);
            this.btExit.TabIndex = 7;
            this.btExit.Text = "Exit";
            this.btExit.UseVisualStyleBackColor = true;
            this.btExit.Click += new System.EventHandler(this.btExit_Click);
            // 
            // btSave
            // 
            this.btSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btSave.Location = new System.Drawing.Point(911, 19);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(80, 30);
            this.btSave.TabIndex = 8;
            this.btSave.Text = "Save";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btPlay
            // 
            this.btPlay.Location = new System.Drawing.Point(90, 19);
            this.btPlay.Name = "btPlay";
            this.btPlay.Size = new System.Drawing.Size(80, 30);
            this.btPlay.TabIndex = 9;
            this.btPlay.Text = "Play";
            this.btPlay.UseVisualStyleBackColor = true;
            this.btPlay.Click += new System.EventHandler(this.btPlay_Click);
            // 
            // btClear
            // 
            this.btClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btClear.Location = new System.Drawing.Point(825, 19);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(80, 30);
            this.btClear.TabIndex = 10;
            this.btClear.Text = "Clear";
            this.btClear.UseVisualStyleBackColor = true;
            this.btClear.Click += new System.EventHandler(this.btClear_Click);
            // 
            // btScoring
            // 
            this.btScoring.Location = new System.Drawing.Point(176, 19);
            this.btScoring.Name = "btScoring";
            this.btScoring.Size = new System.Drawing.Size(80, 30);
            this.btScoring.TabIndex = 11;
            this.btScoring.Text = "Score";
            this.btScoring.UseVisualStyleBackColor = true;
            this.btScoring.Click += new System.EventHandler(this.btScoring_Click);
            // 
            // inkPicture1
            // 
            this.inkPicture1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.inkPicture1.BackColor = System.Drawing.Color.White;
            this.inkPicture1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.inkPicture1.Image = global::PBT.ComplexFigure.Properties.Resources.ComplexFigure;
            this.inkPicture1.InkEnabled = false;
            this.inkPicture1.Location = new System.Drawing.Point(4, 55);
            this.inkPicture1.MarginX = -2147483648;
            this.inkPicture1.MarginY = -2147483648;
            this.inkPicture1.Name = "inkPicture1";
            this.inkPicture1.Size = new System.Drawing.Size(1073, 571);
            this.inkPicture1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.inkPicture1.TabIndex = 5;
            this.inkPicture1.TabStop = false;
            this.inkPicture1.Stroke += new Microsoft.Ink.InkCollectorStrokeEventHandler(this.inkPicture1_Stroke);
            this.inkPicture1.NewPackets += new Microsoft.Ink.InkCollectorNewPacketsEventHandler(this.inkPicture1_NewPackets);
            this.inkPicture1.SizeChanged += new System.EventHandler(this.inkPicture1_SizeChanged);
            this.inkPicture1.Paint += new System.Windows.Forms.PaintEventHandler(this.inkPicture1_Paint);
            // 
            // lbScore
            // 
            this.lbScore.AutoSize = true;
            this.lbScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbScore.Location = new System.Drawing.Point(262, 26);
            this.lbScore.Name = "lbScore";
            this.lbScore.Size = new System.Drawing.Size(14, 16);
            this.lbScore.TabIndex = 12;
            this.lbScore.Text = ": ";
            // 
            // FormComplexFigure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1079, 629);
            this.Controls.Add(this.lbScore);
            this.Controls.Add(this.btScoring);
            this.Controls.Add(this.btClear);
            this.Controls.Add(this.btPlay);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.btExit);
            this.Controls.Add(this.chkShowFigure);
            this.Controls.Add(this.inkPicture1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormComplexFigure";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FormComplexFigure";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.inkPicture1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }  

        #endregion

        private Microsoft.Ink.InkPicture inkPicture1;
        private System.Windows.Forms.CheckBox chkShowFigure;
        private System.Windows.Forms.Button btExit;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Button btPlay;
        private System.Windows.Forms.Button btClear;
        private System.Windows.Forms.Button btScoring;
        private System.Windows.Forms.Label lbScore;
    }
}