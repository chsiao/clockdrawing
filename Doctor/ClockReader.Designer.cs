﻿using System.Windows.Forms;
using Microsoft.Ink;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms.DataVisualization.Charting;

namespace ClockReader
{
    partial class ClockReader
    {

        #region Standard Template Code
        /// Clean up any resources being used.
        /// Windows Forms Designer template code
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        #endregion

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClockReader));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.MenuBar = new System.Windows.Forms.MainMenu(this.components);
            this.FileMenu = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.OpenMenu = new System.Windows.Forms.MenuItem();
            this.saveMenu = new System.Windows.Forms.MenuItem();
            this.menuCapture = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.grpSequence = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.check8 = new System.Windows.Forms.PictureBox();
            this.check10 = new System.Windows.Forms.PictureBox();
            this.check9 = new System.Windows.Forms.PictureBox();
            this.check7 = new System.Windows.Forms.PictureBox();
            this.check6 = new System.Windows.Forms.PictureBox();
            this.check5 = new System.Windows.Forms.PictureBox();
            this.check4 = new System.Windows.Forms.PictureBox();
            this.check3 = new System.Windows.Forms.PictureBox();
            this.check2 = new System.Windows.Forms.PictureBox();
            this.check1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Sec = new System.Windows.Forms.Label();
            this.lbTrial = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbDuration = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbID = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.play_pause = new System.Windows.Forms.Button();
            this.ClockDrawing = new Microsoft.Ink.InkPicture();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.saveClockToImageToolStripMenuItem = new ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lbSequences = new System.Windows.Forms.Label();
            this.lbScore = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.check13 = new System.Windows.Forms.PictureBox();
            this.check12 = new System.Windows.Forms.PictureBox();
            this.check11 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.chartPressure = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartAirTime = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new Microsoft.Ink.InkPicture();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            ((System.ComponentModel.ISupportInitialize)(this.check8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClockDrawing)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check11)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPressure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartAirTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuBar
            // 
            this.MenuBar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.FileMenu});
            // 
            // FileMenu
            // 
            this.FileMenu.Index = 0;
            this.FileMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.OpenMenu,
            this.saveMenu,
            this.menuCapture,
            this.menuItem2});
            this.FileMenu.Text = "&File";
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.Text = "Open A Folder";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // OpenMenu
            // 
            this.OpenMenu.Index = 1;
            this.OpenMenu.Shortcut = System.Windows.Forms.Shortcut.CtrlO;
            this.OpenMenu.Text = "&Open A File";
            this.OpenMenu.Click += new System.EventHandler(this.OpenMenu_Click);
            // 
            // saveMenu
            // 
            this.saveMenu.Index = 2;
            this.saveMenu.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.saveMenu.Text = "&Save";
            this.saveMenu.Click += new System.EventHandler(this.saveMenu_Click);
            // 
            // menuCapture
            // 
            this.menuCapture.Index = 3;
            this.menuCapture.Shortcut = System.Windows.Forms.Shortcut.CtrlI;
            this.menuCapture.Text = "Capture An &Image";
            this.menuCapture.Click += new System.EventHandler(this.menuCapture_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 4;
            this.menuItem2.Text = "&Print";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Participant ID:";
            // 
            // grpSequence
            // 
            this.grpSequence.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSequence.AutoSize = true;
            this.grpSequence.Location = new System.Drawing.Point(6, 57);
            this.grpSequence.Name = "grpSequence";
            this.grpSequence.Size = new System.Drawing.Size(6, 56);
            this.grpSequence.TabIndex = 4;
            this.grpSequence.TabStop = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Scoring Set 1",
            "Scoring Set 2",
            "Scoring Set 3"});
            this.comboBox1.Location = new System.Drawing.Point(6, 4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(279, 21);
            this.comboBox1.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "3. Numbers are in correct order.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(222, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "4. Numbers are drawn w/o rotating the paper.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(184, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "5. Numbers are in the correct postion.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(153, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "6. Numbers are all inside circle.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 186);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(131, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "7. Two hands are present.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 217);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(189, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "8. The hour target number is indicated.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 248);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(199, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "9. The minute target number is indicated.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 279);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(197, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "10. The hands  are in correct proportion.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 310);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(191, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "11. There are no superfluous markings.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 372);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(116, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "13. A center is present.";
            // 
            // check8
            // 
            this.check8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check8.Enabled = false;
            this.check8.Image = ((System.Drawing.Image)(resources.GetObject("check8.Image")));
            this.check8.Location = new System.Drawing.Point(459, 220);
            this.check8.Name = "check8";
            this.check8.Size = new System.Drawing.Size(23, 22);
            this.check8.TabIndex = 32;
            this.check8.TabStop = false;
            this.check8.Visible = false;
            // 
            // check10
            // 
            this.check10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check10.Image = ((System.Drawing.Image)(resources.GetObject("check10.Image")));
            this.check10.Location = new System.Drawing.Point(459, 282);
            this.check10.Name = "check10";
            this.check10.Size = new System.Drawing.Size(23, 23);
            this.check10.TabIndex = 29;
            this.check10.TabStop = false;
            this.check10.Visible = false;
            // 
            // check9
            // 
            this.check9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check9.Image = ((System.Drawing.Image)(resources.GetObject("check9.Image")));
            this.check9.Location = new System.Drawing.Point(459, 251);
            this.check9.Name = "check9";
            this.check9.Size = new System.Drawing.Size(23, 23);
            this.check9.TabIndex = 28;
            this.check9.TabStop = false;
            this.check9.Visible = false;
            // 
            // check7
            // 
            this.check7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check7.Enabled = false;
            this.check7.Image = ((System.Drawing.Image)(resources.GetObject("check7.Image")));
            this.check7.Location = new System.Drawing.Point(459, 189);
            this.check7.Name = "check7";
            this.check7.Size = new System.Drawing.Size(23, 23);
            this.check7.TabIndex = 27;
            this.check7.TabStop = false;
            this.check7.Visible = false;
            // 
            // check6
            // 
            this.check6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check6.Image = ((System.Drawing.Image)(resources.GetObject("check6.Image")));
            this.check6.Location = new System.Drawing.Point(459, 158);
            this.check6.Name = "check6";
            this.check6.Size = new System.Drawing.Size(23, 23);
            this.check6.TabIndex = 26;
            this.check6.TabStop = false;
            this.check6.Visible = false;
            // 
            // check5
            // 
            this.check5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check5.Image = ((System.Drawing.Image)(resources.GetObject("check5.Image")));
            this.check5.Location = new System.Drawing.Point(459, 127);
            this.check5.Name = "check5";
            this.check5.Size = new System.Drawing.Size(23, 23);
            this.check5.TabIndex = 25;
            this.check5.TabStop = false;
            this.check5.Visible = false;
            // 
            // check4
            // 
            this.check4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check4.Image = ((System.Drawing.Image)(resources.GetObject("check4.Image")));
            this.check4.Location = new System.Drawing.Point(459, 96);
            this.check4.Name = "check4";
            this.check4.Size = new System.Drawing.Size(23, 23);
            this.check4.TabIndex = 24;
            this.check4.TabStop = false;
            this.check4.Visible = false;
            // 
            // check3
            // 
            this.check3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check3.Image = ((System.Drawing.Image)(resources.GetObject("check3.Image")));
            this.check3.Location = new System.Drawing.Point(459, 65);
            this.check3.Name = "check3";
            this.check3.Size = new System.Drawing.Size(23, 23);
            this.check3.TabIndex = 23;
            this.check3.TabStop = false;
            this.check3.Visible = false;
            // 
            // check2
            // 
            this.check2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check2.Image = ((System.Drawing.Image)(resources.GetObject("check2.Image")));
            this.check2.Location = new System.Drawing.Point(459, 34);
            this.check2.Name = "check2";
            this.check2.Size = new System.Drawing.Size(23, 23);
            this.check2.TabIndex = 22;
            this.check2.TabStop = false;
            this.check2.Visible = false;
            // 
            // check1
            // 
            this.check1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check1.Image = ((System.Drawing.Image)(resources.GetObject("check1.Image")));
            this.check1.Location = new System.Drawing.Point(459, 3);
            this.check1.Name = "check1";
            this.check1.Size = new System.Drawing.Size(23, 23);
            this.check1.TabIndex = 21;
            this.check1.TabStop = false;
            this.check1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "1. Only numbers 1-12 are present.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 341);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(172, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "12. The hands are relatively joined.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(163, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "2. Only Arabic numbers are used.";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Location = new System.Drawing.Point(3, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Sec);
            this.splitContainer1.Panel1.Controls.Add(this.lbTrial);
            this.splitContainer1.Panel1.Controls.Add(this.label16);
            this.splitContainer1.Panel1.Controls.Add(this.lbDuration);
            this.splitContainer1.Panel1.Controls.Add(this.label15);
            this.splitContainer1.Panel1.Controls.Add(this.lbID);
            this.splitContainer1.Panel1.Controls.Add(this.trackBar1);
            this.splitContainer1.Panel1.Controls.Add(this.play_pause);
            this.splitContainer1.Panel1.Controls.Add(this.ClockDrawing);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(931, 589);
            this.splitContainer1.SplitterDistance = 394;
            this.splitContainer1.TabIndex = 0;
            // 
            // Sec
            // 
            this.Sec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Sec.AutoSize = true;
            this.Sec.Location = new System.Drawing.Point(359, 561);
            this.Sec.Name = "Sec";
            this.Sec.Size = new System.Drawing.Size(13, 13);
            this.Sec.TabIndex = 12;
            this.Sec.Text = "0";
            // 
            // lbTrial
            // 
            this.lbTrial.AutoSize = true;
            this.lbTrial.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTrial.Location = new System.Drawing.Point(127, 53);
            this.lbTrial.Name = "lbTrial";
            this.lbTrial.Size = new System.Drawing.Size(0, 17);
            this.lbTrial.TabIndex = 11;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(4, 53);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(117, 17);
            this.label16.TabIndex = 10;
            this.label16.Text = "Numbers of Trial:";
            // 
            // lbDuration
            // 
            this.lbDuration.AutoSize = true;
            this.lbDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDuration.Location = new System.Drawing.Point(106, 33);
            this.lbDuration.Name = "lbDuration";
            this.lbDuration.Size = new System.Drawing.Size(0, 17);
            this.lbDuration.TabIndex = 9;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 33);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(98, 17);
            this.label15.TabIndex = 8;
            this.label15.Text = "Test Duration:";
            // 
            // lbID
            // 
            this.lbID.AutoSize = true;
            this.lbID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbID.Location = new System.Drawing.Point(106, 13);
            this.lbID.Name = "lbID";
            this.lbID.Size = new System.Drawing.Size(0, 17);
            this.lbID.TabIndex = 7;
            // 
            // trackBar1
            // 
            this.trackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar1.Enabled = false;
            this.trackBar1.Location = new System.Drawing.Point(39, 557);
            this.trackBar1.Maximum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(313, 45);
            this.trackBar1.TabIndex = 6;
            // 
            // play_pause
            // 
            this.play_pause.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.play_pause.BackColor = System.Drawing.Color.White;
            this.play_pause.Image = ((System.Drawing.Image)(resources.GetObject("play_pause.Image")));
            this.play_pause.Location = new System.Drawing.Point(6, 557);
            this.play_pause.Name = "play_pause";
            this.play_pause.Size = new System.Drawing.Size(34, 27);
            this.play_pause.TabIndex = 5;
            this.play_pause.UseVisualStyleBackColor = false;
            this.play_pause.Click += new System.EventHandler(this.play_pause_Click);
            // 
            // ClockDrawing
            // 
            this.ClockDrawing.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ClockDrawing.BackColor = System.Drawing.Color.White;
            this.ClockDrawing.ContextMenuStrip = this.contextMenuStrip1;
            this.ClockDrawing.Location = new System.Drawing.Point(6, 100);
            this.ClockDrawing.MarginX = -2147483648;
            this.ClockDrawing.MarginY = -2147483648;
            this.ClockDrawing.MaximumSize = new System.Drawing.Size(2000, 2000);
            this.ClockDrawing.MinimumSize = new System.Drawing.Size(362, 306);
            this.ClockDrawing.Name = "ClockDrawing";
            this.ClockDrawing.Padding = new System.Windows.Forms.Padding(3);
            this.ClockDrawing.Size = new System.Drawing.Size(379, 454);
            this.ClockDrawing.TabIndex = 4;
            this.ClockDrawing.TabStop = false;
            this.ClockDrawing.SizeChanged += new System.EventHandler(this.ClockDrawing_SizeChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveClockToImageToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(182, 26);
            // 
            // saveClockToImageToolStripMenuItem
            // 
            this.saveClockToImageToolStripMenuItem.Name = "saveClockToImageToolStripMenuItem";
            this.saveClockToImageToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.saveClockToImageToolStripMenuItem.Text = "Save Clock to Image";
            this.saveClockToImageToolStripMenuItem.Click += new System.EventHandler(this.saveClockToImageToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(529, 581);
            this.tabControl1.TabIndex = 21;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.lbSequences);
            this.tabPage1.Controls.Add(this.lbScore);
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Controls.Add(this.comboBox1);
            this.tabPage1.Controls.Add(this.grpSequence);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(521, 555);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Scoring";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lbSequences
            // 
            this.lbSequences.AutoSize = true;
            this.lbSequences.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lbSequences.Location = new System.Drawing.Point(3, 37);
            this.lbSequences.Name = "lbSequences";
            this.lbSequences.Size = new System.Drawing.Size(79, 17);
            this.lbSequences.TabIndex = 21;
            this.lbSequences.Text = "Sequences";
            // 
            // lbScore
            // 
            this.lbScore.AutoSize = true;
            this.lbScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lbScore.Location = new System.Drawing.Point(3, 140);
            this.lbScore.Name = "lbScore";
            this.lbScore.Size = new System.Drawing.Size(52, 17);
            this.lbScore.TabIndex = 20;
            this.lbScore.Text = "Scores";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 257F));
            this.tableLayoutPanel1.Controls.Add(this.check13, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.check12, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.check11, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.check8, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.check9, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.check7, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.check6, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.check5, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.check4, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.check3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.check2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.check1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.check10, 1, 9);
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 160);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.689467F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692544F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692544F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692544F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692544F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692544F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692544F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692544F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692544F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692544F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692544F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692544F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.692544F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(485, 411);
            this.tableLayoutPanel1.TabIndex = 19;
            // 
            // check13
            // 
            this.check13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check13.Image = ((System.Drawing.Image)(resources.GetObject("check13.Image")));
            this.check13.Location = new System.Drawing.Point(459, 375);
            this.check13.Name = "check13";
            this.check13.Size = new System.Drawing.Size(23, 22);
            this.check13.TabIndex = 36;
            this.check13.TabStop = false;
            this.check13.Visible = false;
            // 
            // check12
            // 
            this.check12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check12.Image = ((System.Drawing.Image)(resources.GetObject("check12.Image")));
            this.check12.Location = new System.Drawing.Point(459, 344);
            this.check12.Name = "check12";
            this.check12.Size = new System.Drawing.Size(23, 22);
            this.check12.TabIndex = 35;
            this.check12.TabStop = false;
            this.check12.Visible = false;
            // 
            // check11
            // 
            this.check11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check11.Image = ((System.Drawing.Image)(resources.GetObject("check11.Image")));
            this.check11.Location = new System.Drawing.Point(459, 313);
            this.check11.Name = "check11";
            this.check11.Size = new System.Drawing.Size(23, 22);
            this.check11.TabIndex = 34;
            this.check11.TabStop = false;
            this.check11.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.chartPressure);
            this.tabPage2.Controls.Add(this.chartAirTime);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(521, 555);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Graph";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // chartPressure
            // 
            this.chartPressure.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.AxisX.Interval = 30D;
            chartArea1.AxisX.Title = "Recognized Characters";
            chartArea1.AxisY.Title = "Pressure";
            chartArea1.Name = "ChartArea1";
            this.chartPressure.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartPressure.Legends.Add(legend1);
            this.chartPressure.Location = new System.Drawing.Point(6, 349);
            this.chartPressure.Name = "chartPressure";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series1.CustomProperties = "LabelStyle=Left";
            series1.Legend = "Legend1";
            series1.Name = "Pressure";
            this.chartPressure.Series.Add(series1);
            this.chartPressure.Size = new System.Drawing.Size(432, 300);
            this.chartPressure.TabIndex = 1;
            this.chartPressure.Text = "chart1";
            // 
            // chartAirTime
            // 
            this.chartAirTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.AxisX.Interval = 1D;
            chartArea2.AxisX.Title = "Recognized Characters";
            chartArea2.AxisY.Title = "Milliseconds";
            chartArea2.Name = "ChartArea1";
            this.chartAirTime.ChartAreas.Add(chartArea2);
            legend2.ItemColumnSpacing = 10;
            legend2.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Column;
            legend2.Name = "Legend1";
            this.chartAirTime.Legends.Add(legend2);
            this.chartAirTime.Location = new System.Drawing.Point(6, 6);
            this.chartAirTime.Name = "chartAirTime";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.EmptyPointStyle.CustomProperties = "LabelStyle=Bottom";
            series2.EmptyPointStyle.MarkerSize = 3;
            series2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            series2.IsValueShownAsLabel = true;
            series2.Legend = "Legend1";
            series2.Name = "Air Time ";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
            series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            this.chartAirTime.Series.Add(series2);
            this.chartAirTime.Size = new System.Drawing.Size(432, 337);
            this.chartAirTime.TabIndex = 0;
            this.chartAirTime.Text = "chart1";
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(521, 555);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Monitoring";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(3, 624);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(931, 121);
            this.panel1.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Location = new System.Drawing.Point(8, 10);
            this.pictureBox1.MarginX = -2147483648;
            this.pictureBox1.MarginY = -2147483648;
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(113, 106);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // SerializationForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(939, 753);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitContainer1);
            this.Menu = this.MenuBar;
            this.Name = "SerializationForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Clock Reader Beta 1";
            ((System.ComponentModel.ISupportInitialize)(this.check8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClockDrawing)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check11)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartPressure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartAirTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.MainMenu MenuBar;
        private System.Windows.Forms.MenuItem FileMenu;
        private System.Windows.Forms.MenuItem OpenMenu;

        private System.Windows.Forms.Panel Signature;

        private Label label1;
        private MenuItem saveMenu;
        private GroupBox grpSequence;
        private ComboBox comboBox1;
        private Label label4;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label3;
        private Label label2;
        private Label label14;
        private Label lbTrial;
        private Label label16;
        private Label lbDuration;
        private Label label15;
        private Label lbID;
        private Label Sec;
        private Label lbSequences;
        private Label lbScore;

        private PictureBox check1;
        private PictureBox check8;
        private PictureBox check10;
        private PictureBox check9;
        private PictureBox check7;
        private PictureBox check6;
        private PictureBox check5;
        private PictureBox check4;
        private PictureBox check3;
        private PictureBox check2;
        private InkPicture ClockDrawing;
        private InkPicture pictureBox1;
        

        private MenuItem menuCapture;
        private MenuItem menuItem2;
        private MenuItem menuItem1;
        
        private PrintDialog printDialog1;

        private PictureBox check13;
        private PictureBox check12;
        private PictureBox check11;

        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private TabPage tabPage3;

        private Chart chartPressure;
        private TableLayoutPanel tableLayoutPanel1;

        private Panel panel1;
        private TrackBar trackBar1;
        private Button play_pause;
        private SplitContainer splitContainer1;

        private ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAirTime;
        private ToolStripMenuItem saveClockToImageToolStripMenuItem;
    }
}
