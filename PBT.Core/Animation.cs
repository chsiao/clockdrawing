﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading;

using Microsoft.Ink;

using PBT.Core.TestObject;

namespace PBT.Core
{
    public class Animation
    {
        private static DateTime JanFirst1970 = new DateTime(1970, 1, 1);
        private TestBase testObject = new TestBase();
        private bool isPlaying;
        private InkPicture canvas;
        private int packetPtId = 0;
        private int strokeID = 0;
        private long startTime;
        private long pauseTime;
        private long oriStartTime;
        private Point[] pkPoints;

        private Thread animationThread;
        private Stroke[] animatingStrokes;

        public Animation(TestBase tb, InkPicture inkCanvas)
        {
            this.testObject = tb;
            isPlaying = false;
            this.canvas = inkCanvas;

            animationThread = new Thread(new ThreadStart(StartAnimation));
        }

        public void StartAnimation()
        {
            while (isPlaying)
            {
                ProcessFrame();
            }
        }

        public void start(Stroke[] sts)
        {
            this.animatingStrokes = (Stroke[])sts.Clone();
            if (animationThread != null)
            {
                animationThread.Start();
                isPlaying = true;
            }
        }

        public void pause()
        {
            
        }

        public void stop()
        {
            isPlaying = false;
            animationThread = new Thread(new ThreadStart(StartAnimation));
        }

        private void ProcessFrame()
        {
            try
            {
                if (isPlaying)
                {
                    Stroke[] oriStorkes = this.animatingStrokes;
                    List<PenStroke> strokes = testObject.PenStrokes;
                    using (Graphics g = this.canvas.CreateGraphics())
                    {
                        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                        Renderer r = new Renderer();
                        Ink loadingInk = new Ink();

                        Microsoft.Ink.DrawingAttributes da = new Microsoft.Ink.DrawingAttributes();
                        ExtendedProperties inkProperties = canvas.Ink.ExtendedProperties;


                        if (packetPtId == 0)
                        {

                            if (strokeID == 0)
                            {                                
                                startTime = (long)((DateTime.Now.ToUniversalTime() - JanFirst1970).TotalMilliseconds + 0.5);
                                oriStartTime = strokes[strokeID].PacketPoints[packetPtId].TimeStamp;

                            }
                            long oriPkgPtTime = strokes[strokeID].PacketPoints[packetPtId].TimeStamp;
                            long now = (long)((DateTime.Now.ToUniversalTime() - JanFirst1970).TotalMilliseconds + 0.5);
                            long fromStart = now - startTime;
                            //this.BeginInvoke(new AnimationDelegate(DisplaySec), new object[] { fromStart });

                            // create a new stroke
                            if (now - startTime >= oriPkgPtTime - oriStartTime)
                            {
                                createNewStroke(g, strokes, oriStorkes, inkProperties);
                            }
                        }
                        else
                        {
                            long now = (long)((DateTime.Now.ToUniversalTime() - JanFirst1970).TotalMilliseconds + 0.5);

                            long difference = now - startTime;
                            Microsoft.Ink.Stroke renderingStroke = this.canvas.Ink.Strokes[this.canvas.Ink.Strokes.Count - 1];

                            int tmpPckPtId = 0;
                            int pckPtCount = strokes[strokeID].PacketPoints.Count;
                            System.Drawing.Point finalPktPt = new System.Drawing.Point();
                            while (true)
                            {
                                long oriPkgPtTime = strokes[strokeID].PacketPoints[packetPtId].TimeStamp;

                                if (difference >= oriPkgPtTime - oriStartTime && packetPtId != pckPtCount)
                                {
                                    System.Drawing.Point tmpOriPt = oriStorkes[strokeID].GetPoint(packetPtId);
                                    finalPktPt = new Point(tmpOriPt.X, tmpOriPt.Y);//new System.Drawing.Point((int)(tmpOriPt.X * scale) + translationPt.X, (int)(tmpOriPt.Y * scale) + translationPt.Y);

                                    renderingStroke.SetPoint(packetPtId, finalPktPt);

                                    packetPtId++;
                                    if (packetPtId == pckPtCount) break;
                                }
                                else if (packetPtId != pckPtCount && tmpPckPtId < pckPtCount && finalPktPt != new Point(0, 0))
                                // set the rest of the points in the stroke as the current end point
                                {
                                    if (tmpPckPtId < packetPtId) tmpPckPtId = packetPtId;
                                    renderingStroke.SetPoint(tmpPckPtId, finalPktPt);

                                    tmpPckPtId++;
                                }
                                else
                                    break;
                            }

                            Microsoft.Ink.Stroke tmpStroke = canvas.Ink.Strokes[canvas.Ink.Strokes.Count - 1];
                            Point upperLeft = new System.Drawing.Point(tmpStroke.GetBoundingBox().X, tmpStroke.GetBoundingBox().Y);
                            Point bottomRight = new System.Drawing.Point(tmpStroke.GetBoundingBox().Right, tmpStroke.GetBoundingBox().Bottom);
                            this.canvas.Renderer.InkSpaceToPixel(g, ref upperLeft);
                            this.canvas.Renderer.InkSpaceToPixel(g, ref bottomRight);

                            long fromStart = now - startTime;
                            this.canvas.Invalidate(new Rectangle(upperLeft.X, upperLeft.Y, bottomRight.X, bottomRight.Y));
                            //this.canvas.BeginInvoke(new AnimationDelegate(RefreshAnimation), new object[] { fromStart });
                            

                            if (strokes[strokeID].PacketPoints.Count == packetPtId)
                            {
                                packetPtId = 0;
                                strokeID++;
                                if (strokeID == strokes.Count)
                                {
                                    strokeID = 0;
                                    isPlaying = false;
                                    //Application.Idle -= new EventHandler(Application_Idle);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void createNewStroke(Graphics g, List<PenStroke> strokes, Stroke[] oriStorkes, ExtendedProperties inkProperties)
        {
            pkPoints = new Point[strokes[strokeID].PacketPoints.Count];
            System.Drawing.Point finalPktPt = oriStorkes[strokeID].GetPoint(packetPtId);
            //System.Drawing.Point finalPktPt = new System.Drawing.Point((int)(tmpPt.X * scale) + translationPt.X, (int)(tmpPt.Y * scale) + translationPt.Y);

            int[] pressureValue = new int[strokes[strokeID].PacketPoints.Count];
            for (int i = 0; i < pkPoints.Length; i++)
            {
                pkPoints[i] = finalPktPt;
            }

            Microsoft.Ink.Stroke tmpStroke = canvas.Ink.CreateStroke(pkPoints);// mInkOverly.Ink.CreateStroke(pkPoints);
            if (inkProperties.DoesPropertyExist(PacketProperty.NormalPressure))
            {
                tmpStroke.SetPacketValuesByProperty(PacketProperty.NormalPressure, pressureValue);
            }

            Point upperLeft = new System.Drawing.Point(tmpStroke.GetBoundingBox().X, tmpStroke.GetBoundingBox().Y);
            Point bottomRight = new System.Drawing.Point(tmpStroke.GetBoundingBox().Right, tmpStroke.GetBoundingBox().Bottom);
            this.canvas.Renderer.InkSpaceToPixel(g, ref upperLeft);
            this.canvas.Renderer.InkSpaceToPixel(g, ref bottomRight);

            this.canvas.Invalidate(new Rectangle(upperLeft.X, upperLeft.Y, bottomRight.X, bottomRight.Y));

            packetPtId++;
        }

        private void RefreshAnimation(long sec)
        {
            this.canvas.Refresh();
        }
        private delegate void AnimationDelegate(long sec);
    }
}
